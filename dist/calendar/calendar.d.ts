interface CalendarType {
    source: any;
    jalali: boolean;
    startOfWeek?: number;
    children: any;
}
declare function Calendar({ source, jalali, startOfWeek, children }: CalendarType): any;
export default Calendar;
