import * as React from 'react';
import { DatePickerProps } from './typing';
declare const DatePicker: React.FunctionComponent<DatePickerProps>;
export default DatePicker;
