import * as React from 'react';
import { DatePickerDayProps } from '../typing';
export declare const Day: React.FunctionComponent<DatePickerDayProps>;
