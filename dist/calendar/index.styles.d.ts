export interface Props {
    numberOfMonths?: number;
    jalali?: boolean;
    className?: string;
}
export declare const DayStyle: import("styled-components").StyledComponent<"div", any, {}, never>;
export declare const HeaderStyle: import("styled-components").StyledComponent<"div", any, Props, never>;
export declare const TitleDaysOfWeekStyle: import("styled-components").StyledComponent<"div", any, Props, never>;
export declare const MonthsStyle: import("styled-components").StyledComponent<"div", any, Props, never>;
export declare const DisplayMonthStyle: import("styled-components").StyledComponent<"div", any, Props, never>;
