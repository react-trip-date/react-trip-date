import dayjs, { Dayjs } from 'dayjs';
export declare const dayjsLocalized: (jalali: boolean, date?: string) => Dayjs;
export default dayjs;
