import * as React from 'react';
import { CalendarProps } from './typing';
declare const RangePicker: React.FunctionComponent<CalendarProps>;
export default RangePicker;
