import * as React from 'react';
import { DayProps } from '../typing';
export declare const Day: React.FunctionComponent<DayProps>;
