import React__default, { createContext, useContext, useEffect, createElement, useState, useMemo } from 'react';
import styled from 'styled-components';
import classNames from 'classnames';

var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var dayjs_min = createCommonjsModule(function (module, exports) {
!function(t,e){module.exports=e();}(commonjsGlobal,(function(){var t=1e3,e=6e4,n=36e5,r="millisecond",i="second",s="minute",u="hour",a="day",o="week",f="month",h="quarter",c="year",d="date",$="Invalid Date",l=/^(\d{4})[-/]?(\d{1,2})?[-/]?(\d{0,2})[^0-9]*(\d{1,2})?:?(\d{1,2})?:?(\d{1,2})?[.:]?(\d+)?$/,y=/\[([^\]]+)]|Y{1,4}|M{1,4}|D{1,2}|d{1,4}|H{1,2}|h{1,2}|a|A|m{1,2}|s{1,2}|Z{1,2}|SSS/g,M={name:"en",weekdays:"Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),months:"January_February_March_April_May_June_July_August_September_October_November_December".split("_")},m=function(t,e,n){var r=String(t);return !r||r.length>=e?t:""+Array(e+1-r.length).join(n)+t},g={s:m,z:function(t){var e=-t.utcOffset(),n=Math.abs(e),r=Math.floor(n/60),i=n%60;return (e<=0?"+":"-")+m(r,2,"0")+":"+m(i,2,"0")},m:function t(e,n){if(e.date()<n.date())return -t(n,e);var r=12*(n.year()-e.year())+(n.month()-e.month()),i=e.clone().add(r,f),s=n-i<0,u=e.clone().add(r+(s?-1:1),f);return +(-(r+(n-i)/(s?i-u:u-i))||0)},a:function(t){return t<0?Math.ceil(t)||0:Math.floor(t)},p:function(t){return {M:f,y:c,w:o,d:a,D:d,h:u,m:s,s:i,ms:r,Q:h}[t]||String(t||"").toLowerCase().replace(/s$/,"")},u:function(t){return void 0===t}},D="en",v={};v[D]=M;var p=function(t){return t instanceof _},S=function(t,e,n){var r;if(!t)return D;if("string"==typeof t)v[t]&&(r=t),e&&(v[t]=e,r=t);else {var i=t.name;v[i]=t,r=i;}return !n&&r&&(D=r),r||!n&&D},w=function(t,e){if(p(t))return t.clone();var n="object"==typeof e?e:{};return n.date=t,n.args=arguments,new _(n)},O=g;O.l=S,O.i=p,O.w=function(t,e){return w(t,{locale:e.$L,utc:e.$u,x:e.$x,$offset:e.$offset})};var _=function(){function M(t){this.$L=S(t.locale,null,!0),this.parse(t);}var m=M.prototype;return m.parse=function(t){this.$d=function(t){var e=t.date,n=t.utc;if(null===e)return new Date(NaN);if(O.u(e))return new Date;if(e instanceof Date)return new Date(e);if("string"==typeof e&&!/Z$/i.test(e)){var r=e.match(l);if(r){var i=r[2]-1||0,s=(r[7]||"0").substring(0,3);return n?new Date(Date.UTC(r[1],i,r[3]||1,r[4]||0,r[5]||0,r[6]||0,s)):new Date(r[1],i,r[3]||1,r[4]||0,r[5]||0,r[6]||0,s)}}return new Date(e)}(t),this.$x=t.x||{},this.init();},m.init=function(){var t=this.$d;this.$y=t.getFullYear(),this.$M=t.getMonth(),this.$D=t.getDate(),this.$W=t.getDay(),this.$H=t.getHours(),this.$m=t.getMinutes(),this.$s=t.getSeconds(),this.$ms=t.getMilliseconds();},m.$utils=function(){return O},m.isValid=function(){return !(this.$d.toString()===$)},m.isSame=function(t,e){var n=w(t);return this.startOf(e)<=n&&n<=this.endOf(e)},m.isAfter=function(t,e){return w(t)<this.startOf(e)},m.isBefore=function(t,e){return this.endOf(e)<w(t)},m.$g=function(t,e,n){return O.u(t)?this[e]:this.set(n,t)},m.unix=function(){return Math.floor(this.valueOf()/1e3)},m.valueOf=function(){return this.$d.getTime()},m.startOf=function(t,e){var n=this,r=!!O.u(e)||e,h=O.p(t),$=function(t,e){var i=O.w(n.$u?Date.UTC(n.$y,e,t):new Date(n.$y,e,t),n);return r?i:i.endOf(a)},l=function(t,e){return O.w(n.toDate()[t].apply(n.toDate("s"),(r?[0,0,0,0]:[23,59,59,999]).slice(e)),n)},y=this.$W,M=this.$M,m=this.$D,g="set"+(this.$u?"UTC":"");switch(h){case c:return r?$(1,0):$(31,11);case f:return r?$(1,M):$(0,M+1);case o:var D=this.$locale().weekStart||0,v=(y<D?y+7:y)-D;return $(r?m-v:m+(6-v),M);case a:case d:return l(g+"Hours",0);case u:return l(g+"Minutes",1);case s:return l(g+"Seconds",2);case i:return l(g+"Milliseconds",3);default:return this.clone()}},m.endOf=function(t){return this.startOf(t,!1)},m.$set=function(t,e){var n,o=O.p(t),h="set"+(this.$u?"UTC":""),$=(n={},n[a]=h+"Date",n[d]=h+"Date",n[f]=h+"Month",n[c]=h+"FullYear",n[u]=h+"Hours",n[s]=h+"Minutes",n[i]=h+"Seconds",n[r]=h+"Milliseconds",n)[o],l=o===a?this.$D+(e-this.$W):e;if(o===f||o===c){var y=this.clone().set(d,1);y.$d[$](l),y.init(),this.$d=y.set(d,Math.min(this.$D,y.daysInMonth())).$d;}else $&&this.$d[$](l);return this.init(),this},m.set=function(t,e){return this.clone().$set(t,e)},m.get=function(t){return this[O.p(t)]()},m.add=function(r,h){var d,$=this;r=Number(r);var l=O.p(h),y=function(t){var e=w($);return O.w(e.date(e.date()+Math.round(t*r)),$)};if(l===f)return this.set(f,this.$M+r);if(l===c)return this.set(c,this.$y+r);if(l===a)return y(1);if(l===o)return y(7);var M=(d={},d[s]=e,d[u]=n,d[i]=t,d)[l]||1,m=this.$d.getTime()+r*M;return O.w(m,this)},m.subtract=function(t,e){return this.add(-1*t,e)},m.format=function(t){var e=this;if(!this.isValid())return $;var n=t||"YYYY-MM-DDTHH:mm:ssZ",r=O.z(this),i=this.$locale(),s=this.$H,u=this.$m,a=this.$M,o=i.weekdays,f=i.months,h=function(t,r,i,s){return t&&(t[r]||t(e,n))||i[r].substr(0,s)},c=function(t){return O.s(s%12||12,t,"0")},d=i.meridiem||function(t,e,n){var r=t<12?"AM":"PM";return n?r.toLowerCase():r},l={YY:String(this.$y).slice(-2),YYYY:this.$y,M:a+1,MM:O.s(a+1,2,"0"),MMM:h(i.monthsShort,a,f,3),MMMM:h(f,a),D:this.$D,DD:O.s(this.$D,2,"0"),d:String(this.$W),dd:h(i.weekdaysMin,this.$W,o,2),ddd:h(i.weekdaysShort,this.$W,o,3),dddd:o[this.$W],H:String(s),HH:O.s(s,2,"0"),h:c(1),hh:c(2),a:d(s,u,!0),A:d(s,u,!1),m:String(u),mm:O.s(u,2,"0"),s:String(this.$s),ss:O.s(this.$s,2,"0"),SSS:O.s(this.$ms,3,"0"),Z:r};return n.replace(y,(function(t,e){return e||l[t]||r.replace(":","")}))},m.utcOffset=function(){return 15*-Math.round(this.$d.getTimezoneOffset()/15)},m.diff=function(r,d,$){var l,y=O.p(d),M=w(r),m=(M.utcOffset()-this.utcOffset())*e,g=this-M,D=O.m(this,M);return D=(l={},l[c]=D/12,l[f]=D,l[h]=D/3,l[o]=(g-m)/6048e5,l[a]=(g-m)/864e5,l[u]=g/n,l[s]=g/e,l[i]=g/t,l)[y]||g,$?D:O.a(D)},m.daysInMonth=function(){return this.endOf(f).$D},m.$locale=function(){return v[this.$L]},m.locale=function(t,e){if(!t)return this.$L;var n=this.clone(),r=S(t,e,!0);return r&&(n.$L=r),n},m.clone=function(){return O.w(this.$d,this)},m.toDate=function(){return new Date(this.valueOf())},m.toJSON=function(){return this.isValid()?this.toISOString():null},m.toISOString=function(){return this.$d.toISOString()},m.toString=function(){return this.$d.toUTCString()},M}(),b=_.prototype;return w.prototype=b,[["$ms",r],["$s",i],["$m",s],["$H",u],["$W",a],["$M",f],["$y",c],["$D",d]].forEach((function(t){b[t[1]]=function(e){return this.$g(e,t[0],t[1])};})),w.extend=function(t,e){return t.$i||(t(e,_,w),t.$i=!0),w},w.locale=S,w.isDayjs=p,w.unix=function(t){return w(1e3*t)},w.en=v[D],w.Ls=v,w.p={},w}));
});

var fa = createCommonjsModule(function (module, exports) {
!function(_,e){module.exports=e(dayjs_min);}(commonjsGlobal,(function(_){function e(_){return _&&"object"==typeof _&&"default"in _?_:{default:_}}var t=e(_),d={name:"fa",weekdays:"یک‌شنبه_دوشنبه_سه‌شنبه_چهارشنبه_پنج‌شنبه_جمعه_شنبه".split("_"),weekdaysShort:"یک‌شنبه_دوشنبه_سه‌شنبه_چهارشنبه_پنج‌شنبه_جمعه_شنبه".split("_"),weekdaysMin:"ی_د_س_چ_پ_ج_ش".split("_"),weekStart:6,months:"ژانویه_فوریه_مارس_آوریل_مه_ژوئن_ژوئیه_اوت_سپتامبر_اکتبر_نوامبر_دسامبر".split("_"),monthsShort:"ژانویه_فوریه_مارس_آوریل_مه_ژوئن_ژوئیه_اوت_سپتامبر_اکتبر_نوامبر_دسامبر".split("_"),ordinal:function(_){return _},formats:{LT:"HH:mm",LTS:"HH:mm:ss",L:"DD/MM/YYYY",LL:"D MMMM YYYY",LLL:"D MMMM YYYY HH:mm",LLLL:"dddd, D MMMM YYYY HH:mm"},relativeTime:{future:"در %s",past:"%s پیش",s:"چند ثانیه",m:"یک دقیقه",mm:"%d دقیقه",h:"یک ساعت",hh:"%d ساعت",d:"یک روز",dd:"%d روز",M:"یک ماه",MM:"%d ماه",y:"یک سال",yy:"%d سال"}};return t.default.locale(d,null,!0),d}));
});

function _slicedToArray(arr, i) {
  return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest();
}

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}

function _iterableToArrayLimit(arr, i) {
  var _arr = [];
  var _n = true;
  var _d = false;
  var _e = undefined;

  try {
    for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);

      if (i && _arr.length === i) break;
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i["return"] != null) _i["return"]();
    } finally {
      if (_d) throw _e;
    }
  }

  return _arr;
}

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance");
}

/* eslint-disable */

/*
 JavaScript functions for the Fourmilab Calendar Converter
 by John Walker  --  September, MIM
 http://www.fourmilab.ch/documents/calendar/
 This program is in the public domain.
 */

/*  MOD  --  Modulus function which works for non-integers.  */
var $floor = Math.floor;

function mod(a, b) {
  return a - b * $floor(a / b);
} //  LEAP_GREGORIAN  --  Is a given year in the Gregorian calendar a leap year ?


function lg(year) {
  return year % 4 == 0 && !(year % 100 == 0 && year % 400 != 0);
} //  GREGORIAN_TO_JD  --  Determine Julian day number from Gregorian calendar date
// GREGORIAN_EPOCH


var GE = 1721425.5;

function g2j(year, month, day) {
  return GE - 1 + 365 * (year - 1) + $floor((year - 1) / 4) + -$floor((year - 1) / 100) + $floor((year - 1) / 400) + $floor((367 * month - 362) / 12 + (month <= 2 ? 0 : lg(year) ? -1 : -2) + day);
} //  JD_TO_GREGORIAN  --  Calculate Gregorian calendar date from Julian day


function j2g(jd) {
  var wjd, depoch, quadricent, dqc, cent, dcent, quad, dquad, yindex, year, yearday, leapadj;
  wjd = $floor(jd - 0.5) + 0.5;
  depoch = wjd - GE;
  quadricent = $floor(depoch / 146097);
  dqc = mod(depoch, 146097);
  cent = $floor(dqc / 36524);
  dcent = mod(dqc, 36524);
  quad = $floor(dcent / 1461);
  dquad = mod(dcent, 1461);
  yindex = $floor(dquad / 365);
  year = quadricent * 400 + cent * 100 + quad * 4 + yindex;

  if (!(cent == 4 || yindex == 4)) {
    year++;
  }

  yearday = wjd - g2j(year, 1, 1);
  leapadj = wjd < g2j(year, 3, 1) ? 0 : lg(year) ? 1 : 2;
  var month = $floor(((yearday + leapadj) * 12 + 373) / 367),
      day = wjd - g2j(year, month, 1) + 1;
  return [year, month, day];
} // PERSIAN_EPOCH


var PE = 1948320.5; //  PERSIAN_TO_JD  --  Determine Julian day from Persian date

function p2j(year, month, day) {
  var epbase, epyear;
  epbase = year - (year >= 0 ? 474 : 473);
  epyear = 474 + mod(epbase, 2820);
  return day + (month <= 7 ? (month - 1) * 31 : (month - 1) * 30 + 6) + $floor((epyear * 682 - 110) / 2816) + (epyear - 1) * 365 + $floor(epbase / 2820) * 1029983 + (PE - 1);
} //  JD_TO_PERSIAN  --  Calculate Persian date from Julian day


function j2p(jd) {
  var year, month, day, depoch, cycle, cyear, ycycle, aux1, aux2, yday;
  jd = $floor(jd) + 0.5;
  depoch = jd - p2j(475, 1, 1);
  cycle = $floor(depoch / 1029983);
  cyear = mod(depoch, 1029983);

  if (cyear == 1029982) {
    ycycle = 2820;
  } else {
    aux1 = $floor(cyear / 366);
    aux2 = mod(cyear, 366);
    ycycle = $floor((2134 * aux1 + 2816 * aux2 + 2815) / 1028522) + aux1 + 1;
  }

  year = ycycle + 2820 * cycle + 474;

  if (year <= 0) {
    year--;
  }

  yday = jd - p2j(year, 1, 1) + 1;
  month = yday <= 186 ? Math.ceil(yday / 31) : Math.ceil((yday - 6) / 30);
  day = jd - p2j(year, month, 1) + 1;
  return [year, month, day];
}

var jdate = {
  J: function J(y, m, d) {
    return j2p(g2j(y, m, d));
  },
  G: function G(y, m, d) {
    return j2g(p2j(y, m, d));
  }
};

var REGEX_PARSE = /^(\d{4})[-/]?(\d{1,2})[-/]?(\d{0,2})(.*)?$/;
var REGEX_FORMAT = /\[.*?\]|jY{2,4}|jM{1,4}|jD{1,2}|Y{2,4}|M{1,4}|D{1,2}|d{1,4}|H{1,2}|h{1,2}|a|A|m{1,2}|s{1,2}|Z{1,2}|SSS/g;
var DATE = 'date';
var D = 'day';
var M = 'month';
var Y = 'year';
var W = 'week';
var FORMAT_DEFAULT = 'YYYY-MM-DDTHH:mm:ssZ';
var fa$1 = {
  jmonths: 'فروردین_اردیبهشت_خرداد_تیر_مرداد_شهریور_مهر_آبان_آذر_دی_بهمن_اسفند'.split('_')
};

var plugin = (function (o, Dayjs, dayjs) {
  var proto = Dayjs.prototype;
  var U = proto.$utils();

  var $isJalali = function $isJalali(v) {
    return v.$C === 'jalali';
  };

  var $prettyUnit = U.prettyUnit || U.p;
  var $isUndefined = U.isUndefined || U.u;
  var $padStart = U.padStart || U.s;
  var $monthDiff = U.monthDiff || U.m;
  var $absFloor = U.absFloor || U.a;

  var wrapperOfTruth = function wrapperOfTruth(action) {
    return function () {
      var unsure = action.bind(this).apply(void 0, arguments);
      unsure.$C = this.$C;

      if (unsure.isJalali()) {
        unsure.InitJalali();
      }

      return unsure;
    };
  }; // keep calendar on date manipulation


  proto.startOf = wrapperOfTruth(proto.startOf);
  proto.endOf = wrapperOfTruth(proto.endOf);
  proto.add = wrapperOfTruth(proto.add);
  proto.subtract = wrapperOfTruth(proto.subtract);
  proto.set = wrapperOfTruth(proto.set);
  var oldParse = proto.parse;
  var oldInit = proto.init;
  var oldStartOf = proto.startOf;
  var old$Set = proto.$set;
  var oldAdd = proto.add;
  var oldFormat = proto.format;
  var oldDiff = proto.diff;
  var oldYear = proto.year;
  var oldMonth = proto.month;
  var oldDate = proto.date;
  var oldDaysInMonth = proto.daysInMonth;
  var oldToArray = proto.toArray;
  dayjs.$C = 'gregory'; // First Day Of Week

  dayjs.$fdow = 6; // 0: sunday, ...

  dayjs.calendar = function (calendar) {
    dayjs.$C = calendar;
    return dayjs;
  };

  proto.calendar = function (calendar) {
    var that = this.clone();
    that.$C = calendar;

    if (that.isJalali()) {
      that.InitJalali();
    }

    return that;
  };

  proto.isJalali = function () {
    return $isJalali(this);
  };

  dayjs.en.jmonths = 'Farvardin_Ordibehesht_Khordaad_Tir_Mordaad_Shahrivar_Mehr_Aabaan_Aazar_Dey_Bahman_Esfand'.split('_');
  dayjs.locale('fa', Object.assign({}, fa, {}, fa$1), true);

  var wrapper = function wrapper(date, instance) {
    return dayjs(date, {
      locale: instance.$L,
      utc: instance.$u,
      calendar: instance.$C
    });
  };

  proto.init = function () {
    var cfg = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    oldInit.bind(this)(cfg);

    if (this.isJalali()) {
      this.InitJalali();
    }
  };

  proto.parse = function (cfg) {
    var reg;
    this.$C = cfg.calendar || this.$C || dayjs.$C; // eslint-disable-next-line no-cond-assign

    if (cfg.jalali && typeof cfg.date === 'string' && /.*[^Z]$/i.test(cfg.date) // looking for a better way
    && (reg = cfg.date.match(REGEX_PARSE))) {
      // 1397-08-08 or 13970808
      var _jdate$G = jdate.G(parseInt(reg[1], 10), parseInt(reg[2], 10), parseInt(reg[3] || 1, 10)),
          _jdate$G2 = _slicedToArray(_jdate$G, 3),
          y = _jdate$G2[0],
          m = _jdate$G2[1],
          d = _jdate$G2[2];

      cfg.date = "".concat(y, "-").concat(m, "-").concat(d).concat(reg[4] || '');
    }

    return oldParse.bind(this)(cfg);
  };

  proto.InitJalali = function () {
    var _jdate$J = jdate.J(this.$y, this.$M + 1, this.$D),
        _jdate$J2 = _slicedToArray(_jdate$J, 3),
        jy = _jdate$J2[0],
        jm = _jdate$J2[1],
        jd = _jdate$J2[2];

    this.$jy = jy;
    this.$jM = jm - 1;
    this.$jD = jd;
  };

  proto.startOf = function (units, startOf) {
    var _this = this;

    // startOf -> endOf
    if (!$isJalali(this)) {
      return oldStartOf.bind(this)(units, startOf);
    }

    var isStartOf = !$isUndefined(startOf) ? startOf : true;
    var unit = $prettyUnit(units);

    var instanceFactory = function instanceFactory(d, m) {
      var y = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : _this.$jy;

      var _jdate$G3 = jdate.G(y, m + 1, d),
          _jdate$G4 = _slicedToArray(_jdate$G3, 3),
          gy = _jdate$G4[0],
          gm = _jdate$G4[1],
          gd = _jdate$G4[2];

      var ins = wrapper(new Date(gy, gm - 1, gd), _this);
      return (isStartOf ? ins : ins.endOf(D)).$set('hour', 1); // prevent daylight saving issue in safari
    };

    var WModifier = (this.$W + (7 - dayjs.$fdow)) % 7;

    switch (unit) {
      case Y:
        return isStartOf ? instanceFactory(1, 0) : instanceFactory(0, 0, this.$jy + 1);

      case M:
        return isStartOf ? instanceFactory(1, this.$jM) : instanceFactory(0, (this.$jM + 1) % 12, this.$jy + parseInt((this.$jM + 1) / 12, 10));

      case W:
        return isStartOf ? instanceFactory(this.$jD - WModifier, this.$jM) : instanceFactory(this.$jD + (6 - WModifier), this.$jM);

      default:
        return oldStartOf.bind(this)(units, startOf);
    }
  };

  proto.$set = function (units, _int) {
    var _this2 = this;

    if (!$isJalali(this)) {
      return old$Set.bind(this)(units, _int);
    }

    var unit = $prettyUnit(units);

    var instanceFactory = function instanceFactory(d, m) {
      var y = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : _this2.$jy;

      var _jdate$G5 = jdate.G(y, m + 1, d),
          _jdate$G6 = _slicedToArray(_jdate$G5, 3),
          gy = _jdate$G6[0],
          gm = _jdate$G6[1],
          gd = _jdate$G6[2];

      _this2.$d.setFullYear(gy);

      _this2.$d.setMonth(gm - 1);

      _this2.$d.setDate(gd);

      return _this2;
    };

    switch (unit) {
      case DATE:
      case D:
        instanceFactory(_int, this.$jM);
        break;

      case M:
        instanceFactory(this.$jD, _int);
        break;

      case Y:
        instanceFactory(this.$jD, this.$jM, _int);
        break;

      default:
        return old$Set.bind(this)(units, _int);
    }

    this.init();
    return this;
  };

  proto.add = function (number, units) {
    var _this3 = this;

    if (!$isJalali(this)) {
      return oldAdd.bind(this)(number, units);
    }

    number = Number(number); // eslint-disable-line no-param-reassign
    // units === 'ms' hard code here, will update in next release

    var unit = units && (units.length === 1 || units === 'ms') ? units : $prettyUnit(units);

    var instanceFactory = function instanceFactory(u, n) {
      var date = _this3.set(DATE, 1).set(u, n + number);

      return date.set(DATE, Math.min(_this3.$jD, date.daysInMonth()));
    };

    if (['M', M].indexOf(unit) > -1) {
      var n = this.$jM + number;
      var y = n < 0 ? -Math.ceil(-n / 12) : parseInt(n / 12, 10);
      var d = this.$jD;
      var x = this.set(D, 1).add(y, Y).set(M, n - y * 12);
      return x.set(D, Math.min(x.daysInMonth(), d));
    }

    if (['y', Y].indexOf(unit) > -1) {
      return instanceFactory(Y, this.$jy);
    }

    if (['d', D].indexOf(unit) > -1) {
      var date = new Date(this.$d);
      date.setDate(date.getDate() + number);
      return wrapper(date, this);
    }

    return oldAdd.bind(this)(number, units);
  };

  proto.format = function (formatStr, localeObject) {
    var _this4 = this;

    if (!$isJalali(this)) {
      return oldFormat.bind(this)(formatStr, localeObject);
    }

    var str = formatStr || FORMAT_DEFAULT;
    var locale = localeObject || this.$locale();
    var jmonths = locale.jmonths;
    return str.replace(REGEX_FORMAT, function (match) {
      if (match.indexOf('[') > -1) return match.replace(/\[|\]/g, '');

      switch (match) {
        case 'YY':
          return String(_this4.$jy).slice(-2);

        case 'YYYY':
          return String(_this4.$jy);

        case 'M':
          return String(_this4.$jM + 1);

        case 'MM':
          return $padStart(_this4.$jM + 1, 2, '0');

        case 'MMM':
          return jmonths[_this4.$jM].slice(0, 3);

        case 'MMMM':
          return jmonths[_this4.$jM];

        case 'D':
          return String(_this4.$jD);

        case 'DD':
          return $padStart(_this4.$jD, 2, '0');

        default:
          return oldFormat.bind(_this4)(match, localeObject);
      }
    });
  };

  proto.diff = function (input, units, _float) {
    if (!$isJalali(this)) {
      return oldDiff.bind(this)(input, units, _float);
    }

    var unit = $prettyUnit(units);
    var that = dayjs(input);
    var result = $monthDiff(this, that);

    switch (unit) {
      case Y:
        result /= 12;
        break;

      case M:
        break;

      default:
        // milliseconds
        return oldDiff.bind(this)(input, units, _float);
    }

    return _float ? result : $absFloor(result);
  };

  proto.$g = function (input, get, set) {
    if ($isUndefined(input)) return this[get];
    return this.set(set, input);
  };

  proto.year = function (input) {
    if (!$isJalali(this)) {
      return oldYear.bind(this)(input);
    }

    return this.$g(input, '$jy', Y);
  };

  proto.month = function (input) {
    if (!$isJalali(this)) {
      return oldMonth.bind(this)(input);
    }

    return this.$g(input, '$jM', M);
  };

  proto.date = function (input) {
    if (!$isJalali(this)) {
      return oldDate.bind(this)(input);
    }

    return this.$g(input, '$jD', D);
  };

  proto.daysInMonth = function () {
    if (!$isJalali(this)) {
      return oldDaysInMonth.bind(this)();
    }

    return this.endOf(M).$jD;
  };
  /**
   * toArray function moved to official plugin
   * Check function existence before override
   */


  if (oldToArray) {
    proto.toArray = function () {
      if (!$isJalali(this)) {
        return oldToArray.bind(this)();
      }

      return [this.$jy, this.$jM, this.$jD, this.$H, this.$m, this.$s, this.$ms];
    };
  }

  proto.clone = function () {
    return wrapper(this.toDate(), this);
  };
});

var isBetween = createCommonjsModule(function (module, exports) {
!function(e,i){module.exports=i();}(commonjsGlobal,(function(){return function(e,i,t){i.prototype.isBetween=function(e,i,s,f){var n=t(e),o=t(i),r="("===(f=f||"()")[0],u=")"===f[1];return (r?this.isAfter(n,s):!this.isBefore(n,s))&&(u?this.isBefore(o,s):!this.isAfter(o,s))||(r?this.isBefore(n,s):!this.isAfter(n,s))&&(u?this.isAfter(o,s):!this.isBefore(o,s))};}}));
});

var isSameOrBefore = createCommonjsModule(function (module, exports) {
!function(e,i){module.exports=i();}(commonjsGlobal,(function(){return function(e,i){i.prototype.isSameOrBefore=function(e,i){return this.isSame(e,i)||this.isBefore(e,i)};}}));
});

dayjs_min.extend(plugin);
dayjs_min.extend(isBetween);
dayjs_min.extend(isSameOrBefore);
var dayjsLocalized = function (jalali, date) {
    if (date) {
        return dayjs_min(date)
            .calendar(jalali ? 'jalali' : 'gregory')
            .locale(jalali ? 'fa' : 'en');
    }
    return dayjs_min()
        .calendar(jalali ? 'jalali' : 'gregory')
        .locale(jalali ? 'fa' : 'en');
};

/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

/** @deprecated */
function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
}

var DayStyle = styled.div(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  width: 40px;\n  height: 40px;\n  text-align: center;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  transition: all 0.1s ease-in-out;\n  margin-bottom: 5px;\n  flex-direction: column;\n  cursor: pointer;\n\n  p {\n    margin: 0;\n  }\n\n  .price {\n    font-size: 0.42rem;\n  }\n\n  .date {\n    font-size: ", ";\n  }\n\n  &.inactive {\n    color: transparent;\n    visibility: hidden;\n  }\n\n  &.disabled {\n    color: ", ";\n    position: relative;\n\n    &:hover {\n      cursor: not-allowed;\n    }\n\n    &::after {\n      position: absolute;\n      content: '';\n      width: 15px;\n      height: 2px;\n      background-color: ", ";\n      transform: rotate(-20deg);\n    }\n  }\n\n  &.range-select {\n    background-color: ", ";\n    color: #fff;\n\n    filter: drop-shadow(4px 0px 2px rgba(0, 0, 0, 0.1))\n      drop-shadow(4px 0px 2px rgba(0, 0, 0, 0.1));\n\n    &.jalali {\n      filter: drop-shadow(-4px 0px 2px rgba(0, 0, 0, 0.1))\n        drop-shadow(-4px 0px 2px rgba(0, 0, 0, 0.1));\n    }\n\n    &.end-date {\n      border-top-right-radius: 25px;\n      border-bottom-right-radius: 25px;\n\n      &.jalali {\n        border-top-left-radius: 25px;\n        border-bottom-left-radius: 25px;\n        border-top-right-radius: 0px;\n        border-bottom-right-radius: 0px;\n      }\n    }\n\n    &.start-date {\n      border-top-left-radius: 25px;\n      border-bottom-left-radius: 25px;\n\n      &.jalali {\n        border-top-left-radius: 0px;\n        border-bottom-left-radius: 0px;\n        border-top-right-radius: 25px;\n        border-bottom-right-radius: 25px;\n      }\n    }\n  }\n\n  &.select-mode {\n    border-radius: 50%;\n    margin-left: 5px;\n    margin-bottom: 5px;\n    width: 38px;\n    height: 38px;\n    transition: all 0.3s ease-in-out;\n\n    &:hover {\n      background-color: ", ";\n    }\n  }\n\n  &.selected {\n    color: #fff;\n    background-color: ", ";\n    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.2);\n\n    &:hover {\n      background-color: ", ";\n    }\n\n    p {\n      color: #fff;\n    }\n\n    &::after {\n      display: none;\n    }\n  }\n  &.same {\n    border-top-left-radius: 25px !important;\n    border-bottom-left-radius: 25px !important;\n    border-top-right-radius: 25px !important;\n    border-bottom-right-radius: 25px !important;\n  }\n"], ["\n  width: 40px;\n  height: 40px;\n  text-align: center;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  transition: all 0.1s ease-in-out;\n  margin-bottom: 5px;\n  flex-direction: column;\n  cursor: pointer;\n\n  p {\n    margin: 0;\n  }\n\n  .price {\n    font-size: 0.42rem;\n  }\n\n  .date {\n    font-size: ", ";\n  }\n\n  &.inactive {\n    color: transparent;\n    visibility: hidden;\n  }\n\n  &.disabled {\n    color: ", ";\n    position: relative;\n\n    &:hover {\n      cursor: not-allowed;\n    }\n\n    &::after {\n      position: absolute;\n      content: '';\n      width: 15px;\n      height: 2px;\n      background-color: ", ";\n      transform: rotate(-20deg);\n    }\n  }\n\n  &.range-select {\n    background-color: ", ";\n    color: #fff;\n\n    filter: drop-shadow(4px 0px 2px rgba(0, 0, 0, 0.1))\n      drop-shadow(4px 0px 2px rgba(0, 0, 0, 0.1));\n\n    &.jalali {\n      filter: drop-shadow(-4px 0px 2px rgba(0, 0, 0, 0.1))\n        drop-shadow(-4px 0px 2px rgba(0, 0, 0, 0.1));\n    }\n\n    &.end-date {\n      border-top-right-radius: 25px;\n      border-bottom-right-radius: 25px;\n\n      &.jalali {\n        border-top-left-radius: 25px;\n        border-bottom-left-radius: 25px;\n        border-top-right-radius: 0px;\n        border-bottom-right-radius: 0px;\n      }\n    }\n\n    &.start-date {\n      border-top-left-radius: 25px;\n      border-bottom-left-radius: 25px;\n\n      &.jalali {\n        border-top-left-radius: 0px;\n        border-bottom-left-radius: 0px;\n        border-top-right-radius: 25px;\n        border-bottom-right-radius: 25px;\n      }\n    }\n  }\n\n  &.select-mode {\n    border-radius: 50%;\n    margin-left: 5px;\n    margin-bottom: 5px;\n    width: 38px;\n    height: 38px;\n    transition: all 0.3s ease-in-out;\n\n    &:hover {\n      background-color: ", ";\n    }\n  }\n\n  &.selected {\n    color: #fff;\n    background-color: ", ";\n    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.2);\n\n    &:hover {\n      background-color: ", ";\n    }\n\n    p {\n      color: #fff;\n    }\n\n    &::after {\n      display: none;\n    }\n  }\n  &.same {\n    border-top-left-radius: 25px !important;\n    border-bottom-left-radius: 25px !important;\n    border-top-right-radius: 25px !important;\n    border-bottom-right-radius: 25px !important;\n  }\n"])), function (props) { return props.theme.fs11; }, function (props) { return props.theme.disabledText; }, function (props) { return props.theme.disabledText; }, function (props) { return props.theme.primary; }, function (props) { return props.theme.underlineColor; }, function (props) { return props.theme.primary; }, function (props) { return props.theme.primary; });
var HeaderStyle = styled.div(templateObject_2 || (templateObject_2 = __makeTemplateObject(["\n  display: flex;\n  flex-direction: ", ";\n  justify-content: space-between;\n  align-items: center;\n  height: 55px;\n  background-color: ", ";\n  position: relative;\n\n  p {\n    direction: ", ";\n    font-size: ", ";\n    color: #fff;\n    width: ", ";\n    text-align: center;\n    cursor: pointer;\n  }\n  .action {\n    height: 55px;\n    width: 55px;\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    cursor: pointer;\n    transform: ", ";\n  }\n\n  path {\n    fill: #fff;\n  }\n"], ["\n  display: flex;\n  flex-direction: ", ";\n  justify-content: space-between;\n  align-items: center;\n  height: 55px;\n  background-color: ", ";\n  position: relative;\n\n  p {\n    direction: ", ";\n    font-size: ", ";\n    color: #fff;\n    width: ", ";\n    text-align: center;\n    cursor: pointer;\n  }\n  .action {\n    height: 55px;\n    width: 55px;\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    cursor: pointer;\n    transform: ", ";\n  }\n\n  path {\n    fill: #fff;\n  }\n"])), function (props) { return (props.jalali ? 'row-reverse' : 'row'); }, function (props) { return props.theme.primary; }, function (props) { return (props.jalali ? 'ltr' : 'rtl'); }, function (props) { return props.theme.fs13; }, function (props) { return 100 / props.numberOfMonths + "%"; }, function (props) { return (props.jalali ? 'rotate(0deg)' : 'rotate(180deg)'); });
var TitleDaysOfWeekStyle = styled.div(templateObject_3 || (templateObject_3 = __makeTemplateObject(["\n  display: flex;\n  text-align: center;\n  justify-content: center;\n  flex-direction: ", ";\n  border-bottom: 1px solid #e2e2e2;\n  margin: 30px 0 15px 0;\n\n  p {\n    width: 40px;\n    margin-bottom: 15px;\n  }\n"], ["\n  display: flex;\n  text-align: center;\n  justify-content: center;\n  flex-direction: ", ";\n  border-bottom: 1px solid #e2e2e2;\n  margin: 30px 0 15px 0;\n\n  p {\n    width: 40px;\n    margin-bottom: 15px;\n  }\n"])), function (props) { return (props.jalali ? 'row-reverse' : 'row'); });
var MonthsStyle = styled.div(templateObject_4 || (templateObject_4 = __makeTemplateObject(["\n  display: flex;\n  flex-direction: ", ";\n  justify-content: center;\n  margin: 0 15px;\n  .month {\n    width: ", ";\n    margin: 0 15px;\n  }\n\n  .week {\n    display: flex;\n    flex-direction: ", ";\n    justify-content: center;\n  }\n"], ["\n  display: flex;\n  flex-direction: ", ";\n  justify-content: center;\n  margin: 0 15px;\n  .month {\n    width: ", ";\n    margin: 0 15px;\n  }\n\n  .week {\n    display: flex;\n    flex-direction: ", ";\n    justify-content: center;\n  }\n"])), function (props) { return (props.jalali ? 'row-reverse' : 'row'); }, function (props) { return 100 / props.numberOfMonths + "%"; }, function (props) { return (props.jalali ? 'row-reverse' : 'row'); });
var DisplayMonthStyle = styled.div(templateObject_5 || (templateObject_5 = __makeTemplateObject(["\n  margin-top: 15px;\n  display: flex;\n  flex: 1 1;\n  height: 350px;\n  flex-wrap: wrap;\n  flex-direction: ", ";\n\n  .month {\n    flex-basis: calc(25% - 4px);\n    justify-content: center;\n    text-align: center;\n    display: flex;\n    background-color: ", ";\n    align-items: center;\n    cursor: pointer;\n    border: 2px solid #fff;\n\n    p {\n      color: #fff;\n    }\n  }\n"], ["\n  margin-top: 15px;\n  display: flex;\n  flex: 1 1;\n  height: 350px;\n  flex-wrap: wrap;\n  flex-direction: ", ";\n\n  .month {\n    flex-basis: calc(25% - 4px);\n    justify-content: center;\n    text-align: center;\n    display: flex;\n    background-color: ", ";\n    align-items: center;\n    cursor: pointer;\n    border: 2px solid #fff;\n\n    p {\n      color: #fff;\n    }\n  }\n"])), function (props) { return (props.jalali ? 'row-reverse' : 'row'); }, function (props) { return props.theme.primary; });
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }
var ChevronRight = (({
  styles = {},
  ...props
}) => /*#__PURE__*/React__default.createElement("svg", _extends({
  xmlns: "http://www.w3.org/2000/svg",
  width: "4.877",
  height: "7.91"
}, props), /*#__PURE__*/React__default.createElement("path", {
  d: "M-.001 6.986L3.03 3.955-.001.924.922.001l3.954 3.954L.922 7.909z",
  fill: "#757575"
})));

function _extends$1() { _extends$1 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$1.apply(this, arguments); }
var ChevronLeft = (({
  styles = {},
  ...props
}) => /*#__PURE__*/React__default.createElement("svg", _extends$1({
  xmlns: "http://www.w3.org/2000/svg",
  width: "4.876",
  height: "7.908"
}, props), /*#__PURE__*/React__default.createElement("path", {
  d: "M4.877.924L1.846 3.955l3.031 3.031-.923.923L0 3.955 3.954.001z",
  fill: "#757575"
})));

var Context = createContext(null);

var Header = function () {
    var _a = useContext(Context), source = _a.source, setSource = _a.setSource, displayMonths = _a.displayMonths, jalali = _a.jalali, numberOfMonths = _a.numberOfMonths, setDisplayMonths = _a.setDisplayMonths;
    var nextMonth = function () {
        if (displayMonths) {
            setSource(source.add(1, 'year'));
        }
        else {
            setSource(source.add(1, 'month'));
        }
    };
    var prevMonth = function () {
        if (displayMonths) {
            setSource(source.subtract(1, 'year'));
        }
        else {
            setSource(source.subtract(1, 'month'));
        }
    };
    useEffect(function () { }, [displayMonths]);
    var titleMonth = function () {
        var titles = [];
        for (var i = 0; i < numberOfMonths; i++) {
            if (source.get('day') === 0) {
                source = source.add(1, 'day');
            }
            titles.push(createElement("p", { key: Math.random(), onClick: function () { return setDisplayMonths(true); } }, source.add(i, 'month').format('YYYY-MMMM')));
        }
        return titles;
    };
    return (createElement(HeaderStyle, { numberOfMonths: numberOfMonths, jalali: jalali },
        createElement("div", { className: "action", onClick: prevMonth },
            createElement(ChevronRight, { className: 'prev-month' }),
            displayMonths ? createElement(ChevronRight, { className: 'prev-month' }) : null),
        titleMonth(),
        createElement("div", { className: "action", onClick: nextMonth },
            createElement(ChevronLeft, { className: 'next-month' }),
            displayMonths ? createElement(ChevronLeft, { className: 'next-month' }) : null)));
};

function createCalendar(_a) {
    var source = _a.source, daysInMonth = _a.daysInMonth, firstDay = _a.firstDay, startOfWeek = _a.startOfWeek;
    var days = [];
    var shift = firstDay === startOfWeek
        ? 0
        : firstDay + (startOfWeek === 0 ? 0 : 7 - startOfWeek);
    var tmpDay = source.startOf('month').subtract(shift, 'day');
    var totalDays = daysInMonth + shift;
    var remaining = totalDays % 7;
    for (var i = 0; i < totalDays + (remaining > 0 ? 7 - remaining : 0); i++) {
        days.push(tmpDay);
        tmpDay = tmpDay.add(1, 'day');
    }
    return days;
}
var slice = function (arr, len) {
    var i = 0;
    var tmp = [];
    while (i * len < arr.length) {
        tmp.push(arr.slice(i * len, ++i * len));
    }
    return tmp;
};
function Calendar(_a) {
    var source = _a.source, jalali = _a.jalali, _b = _a.startOfWeek, startOfWeek = _b === void 0 ? 0 : _b, children = _a.children;
    var daysInMonth = source.daysInMonth(), firstDay = source.startOf('month').day();
    if (source.get('day') === 0) {
        source = source.add(1, 'day');
    }
    var groupedDays = slice(createCalendar({
        source: source,
        daysInMonth: daysInMonth,
        firstDay: firstDay,
        startOfWeek: jalali ? 6 : startOfWeek,
    }), 7);
    return children(groupedDays);
}

// import { JustifyContent, AlignItems } from "csstype";
var Box = styled.div(function (props) { return ({
    display: "flex",
    flexDirection: "" + (props.column ? "column" : "row") + (props.reverse ? "-reverse" : ""),
    // justifyContent: props.justifyContent || undefined,
    // alignItems: props.alignItems || undefined,
    order: props.order || undefined,
    flex: props.flex || undefined,
    flexWrap: props.flexWrap ? "wrap" : undefined
}); });

var TitleOfWeek = function (_a) {
    var jalali = _a.jalali;
    var titleDayFa = ['ش', 'ی', 'د', 'س', 'چ', 'پ', 'ج'];
    var titleDayEn = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];
    var title = jalali ? titleDayFa : titleDayEn;
    return (createElement(TitleDaysOfWeekStyle, { jalali: jalali, className: "title-weeks" }, title.map(function (item, index) { return (createElement("p", { key: "title-days-of-weeks" + index }, item)); })));
};

var convertDatesToArray = function (selectedDate, toJalali) {
    return selectedDate.map(function (item) {
        return dayjs_min(item, { jalali: !toJalali })
            .calendar(toJalali ? 'jalali' : 'gregory')
            .format('YYYY-MM-DD');
    });
};

var Day = function (_a) {
    var day = _a.day, month = _a.month;
    var _b = useContext(Context), source = _b.source, handleChange = _b.handleChange, selectedDays = _b.selectedDays, disabledBeforToday = _b.disabledBeforToday, setSelectedDays = _b.setSelectedDays, jalali = _b.jalali, disabled = _b.disabled, disabledDays = _b.disabledDays, numberOfSelectableDays = _b.numberOfSelectableDays, DayComponent = _b.DayComponent;
    var handleClick = function (day) {
        var date = day.currentTarget.getAttribute('data-date');
        if (disabled)
            return false;
        if ((disabledBeforToday &&
            dayjs_min(date).isBefore(dayjs_min()
                .calendar(jalali ? 'jalali' : 'gregory')
                .format('YYYY-MM-DD'))) ||
            disabledDays.includes(dayjs_min(date, { jalali: jalali })
                .calendar(jalali ? 'gregory' : '')
                .format('YYYY-MM-DD'))) {
            var dates = selectedDays.filter(function (item) {
                return item !== date;
            });
            setSelectedDays(dates);
            handleChange(dates);
            return false;
        }
        if (numberOfSelectableDays) {
            if (numberOfSelectableDays === 1) {
                setSelectedDays([date]);
                return false;
            }
            if (selectedDays.length < numberOfSelectableDays &&
                numberOfSelectableDays > 0) {
                // user could selected just size of multipleDate
                if (selectedDays.includes(date)) {
                    var dates = selectedDays.filter(function (item) {
                        return item !== date;
                    });
                    setSelectedDays(dates);
                    if (jalali) {
                        handleChange(convertDatesToArray(dates, !jalali));
                    }
                    else {
                        handleChange(dates);
                    }
                }
                else {
                    setSelectedDays(__spreadArrays(selectedDays, [date]));
                    if (jalali) {
                        handleChange(convertDatesToArray(selectedDays.concat([date]), !jalali));
                    }
                    else {
                        handleChange(__spreadArrays(selectedDays, [date]));
                    }
                }
            }
            else {
                // otherwise user must remove extra date selected
                // (if selectedDate default more than multipleDate)
                if (selectedDays.includes(date)) {
                    var dates = selectedDays.filter(function (item) {
                        return item !== date;
                    });
                    setSelectedDays(dates);
                    if (jalali) {
                        handleChange(convertDatesToArray(dates, !jalali));
                    }
                    else {
                        handleChange(dates);
                    }
                }
            }
        }
        else {
            // if doesn't multipleDate user could choose a lot and remove
            if (selectedDays.includes(date)) {
                var dates = selectedDays.filter(function (item) {
                    return item !== date;
                });
                setSelectedDays(__spreadArrays(dates));
                if (jalali) {
                    handleChange(convertDatesToArray(dates, !jalali));
                }
                else {
                    handleChange(dates);
                }
            }
            else {
                setSelectedDays(selectedDays.concat([date]));
                if (jalali) {
                    handleChange(convertDatesToArray(selectedDays.concat([date]), !jalali));
                }
                else {
                    handleChange(selectedDays.concat([date]));
                }
            }
        }
        return false;
    };
    var handleSelectedDate = function (day) {
        var date = dayjs_min(day, { jalali: jalali }, 'YYYY-MM-DD')
            .calendar(jalali ? 'jalali' : 'gregory')
            .format('YYYY-MM-DD');
        return selectedDays.includes(date);
    };
    var handleDisabledDate = function (day) {
        var date = dayjs_min(day).format('YYYY-MM-DD');
        if (jalali) {
            date = dayjs_min(day, { jalali: jalali }, 'YYYY-MM-DD')
                .calendar('gregory')
                .format('YYYY-MM-DD');
        }
        return (disabledDays.includes(date) ||
            (disabledBeforToday && dayjs_min(date).isBefore(dayjs_min().format('YYYY-MM-DD'))));
    };
    return (createElement(DayStyle, { className: classNames({
            'select-mode': true,
            inactive: day.month() !== source.subtract(-month, 'month').month(),
            selected: handleSelectedDate(day),
            disabled: handleDisabledDate(day),
            today: dayjs_min()
                .calendar(jalali ? 'jalali' : 'gregory')
                .format('YYYY-MM-DD') === day.format('YYYY-MM-DD'),
        }), "data-date": day.format('YYYY-MM-DD'), onClick: function (day) { return handleClick(day); } }, DayComponent ? (createElement(DayComponent, { day: day })) : (createElement("p", { className: "date" }, day.format('DD')))));
};

var Month = function () {
    var _a = useContext(Context), source = _a.source, jalali = _a.jalali, numberOfMonths = _a.numberOfMonths, TitleComponent = _a.TitleComponent;
    var months = [];
    var _loop_1 = function (i) {
        if (source.get('day') === 0) {
            source = source.add(1, 'day');
        }
        months.push(createElement("div", { className: "month date-picker", key: i },
            TitleComponent ? (createElement(TitleComponent, { source: source })) : (createElement(TitleOfWeek, { jalali: jalali })),
            createElement(Calendar, { source: source.subtract(-i, 'month'), jalali: jalali }, function (calendar) { return (createElement(Box, { column: true }, calendar.map(function (week) { return (createElement(Box, { className: "week", key: Math.random() }, week.map(function (day) {
                return createElement(Day, { key: Math.random(), day: day, month: i });
            }))); }))); })));
    };
    for (var i = 0; i < numberOfMonths; i++) {
        _loop_1(i);
    }
    return (createElement(MonthsStyle, { numberOfMonths: numberOfMonths || 1, jalali: jalali }, months));
};

var DisplayMonth = function () {
    var _a = useContext(Context), source = _a.source, setSource = _a.setSource, setDisplayMonths = _a.setDisplayMonths, jalali = _a.jalali;
    var _b = useState([]), months = _b[0], setMonths = _b[1];
    useEffect(function () {
        var months = [];
        for (var i = 0; i < 12; i++) {
            months[i] = source.month(i);
        }
        setMonths(months);
    }, [jalali, source]);
    var updateMonth = function (source) {
        if (source.get('day') === 0) {
            source = source.add(1, 'day');
        }
        setSource(source.month(+source.subtract(1, 'month').format('M')));
        setDisplayMonths(false);
    };
    return (createElement(DisplayMonthStyle, { jalali: jalali, className: "months" }, months.map(function (item) {
        return (createElement("div", { className: "month", key: item.format('MMMM'), onClick: function () { return updateMonth(item); } },
            createElement("p", null, item.format('MMMM'))));
    })));
};

var DatePicker = function (_a) {
    var _b = _a.jalali, jalali = _b === void 0 ? false : _b, _c = _a.numberOfMonths, numberOfMonths = _c === void 0 ? 1 : _c, handleChange = _a.handleChange, _d = _a.selectedDays, selectedDays = _d === void 0 ? [] : _d, _e = _a.numberOfSelectableDays, numberOfSelectableDays = _e === void 0 ? 0 : _e, _f = _a.disabledDays, disabledDays = _f === void 0 ? [] : _f, _g = _a.responsive, responsive = _g === void 0 ? undefined : _g, _h = _a.disabled, disabled = _h === void 0 ? false : _h, _j = _a.disabledBeforToday, disabledBeforToday = _j === void 0 ? true : _j, dayComponent = _a.dayComponent, titleComponent = _a.titleComponent;
    var _k = useState(selectedDays), cloneselectedDays = _k[0], setSelectedDays = _k[1];
    var _l = useState(numberOfMonths), numberOfMonth = _l[0], setNumberOfMonth = _l[1];
    var _m = useState(false), displayMonths = _m[0], setDisplayMonths = _m[1];
    var _o = useState(dayjsLocalized(jalali)), source = _o[0], setSource = _o[1];
    useEffect(function () {
        if (selectedDays.length > 0) {
            var firstDate = selectedDays.sort(function (prev, next) {
                return dayjs_min(prev).isSameOrBefore(next) ? -1 : 1;
            })[0];
            var diffDay = 0;
            diffDay = dayjs_min(firstDate).diff(dayjs_min().format('YYYY-MM-DD'), 'day');
            setSource(dayjs_min()
                .add(diffDay, 'day')
                .calendar(jalali ? 'jalali' : 'gregory')
                .locale(jalali ? 'fa' : 'en'));
        }
        else {
            setSource(dayjsLocalized(jalali));
        }
    }, [jalali]);
    useEffect(function () {
        if (selectedDays) {
            if (jalali) {
                setSelectedDays(convertDatesToArray(selectedDays, jalali));
            }
            else {
                setSelectedDays(selectedDays);
            }
        }
    }, []);
    useEffect(function () {
        var handleResize = function () {
            responsive(setNumberOfMonth);
        };
        if (responsive) {
            responsive(setNumberOfMonth);
            if (typeof window !== 'undefined') {
                //handle ssr
                window.addEventListener('resize', handleResize);
            }
        }
        else {
            setNumberOfMonth(numberOfMonths);
        }
        return function () {
            if (typeof window !== 'undefined') {
                window.removeEventListener('resize', handleResize);
            }
        };
    }, [numberOfMonths]);
    return (createElement("div", { className: "tp-calendar" },
        createElement(Context.Provider, { value: {
                handleChange: handleChange,
                selectedDays: cloneselectedDays,
                disabledDays: disabledDays,
                displayMonths: displayMonths,
                setDisplayMonths: setDisplayMonths,
                numberOfSelectableDays: numberOfSelectableDays,
                disabled: disabled,
                disabledBeforToday: disabledBeforToday,
                setSelectedDays: setSelectedDays,
                numberOfMonths: numberOfMonth,
                jalali: jalali,
                source: source,
                setSource: setSource,
                DayComponent: dayComponent,
                TitleComponent: titleComponent,
            } },
            createElement(Header, null),
            displayMonths ? createElement(DisplayMonth, null) : createElement(Month, null))));
};

var Day$1 = function (_a) {
    var day = _a.day, month = _a.month;
    var _b = useContext(Context), handleChange = _b.handleChange, selectedDays = _b.selectedDays, disabledDays = _b.disabledDays, disabled = _b.disabled, setSelectedDays = _b.setSelectedDays, jalali = _b.jalali, source = _b.source, stateOfRange = _b.stateOfRange, setStateOfRange = _b.setStateOfRange, hoverDay = _b.hoverDay, setHoverDay = _b.setHoverDay, firstClick = _b.firstClick, setFirstClick = _b.setFirstClick, hoverable = _b.hoverable, disabledBeforToday = _b.disabledBeforToday, DayComponent = _b.DayComponent;
    var handleClick = function (day) { return __awaiter(void 0, void 0, void 0, function () {
        var date, disables;
        return __generator(this, function (_a) {
            date = day;
            if (disabledDays) {
                if ((disabledBeforToday &&
                    dayjs_min(date, { jalali: jalali }, 'YYYY-MM-DD').isBefore(dayjsLocalized(jalali), 'day')) ||
                    disabledDays.includes(dayjs_min(date, { jalali: jalali })
                        .calendar(jalali ? 'gregory' : '')
                        .format('YYYY-MM-DD'))) {
                    return [2 /*return*/, false];
                }
            }
            if (disabled)
                return [2 /*return*/, false];
            if (!stateOfRange) {
                setSelectedDays(__assign({ from: '' }, selectedDays));
            }
            if (firstClick) {
                setSelectedDays(__assign(__assign({}, selectedDays), { from: date }));
                setHoverDay(date);
                setFirstClick(false);
                setStateOfRange(true);
            }
            else {
                if (disabledDays) {
                    disables = disabledDays.filter(function (item) {
                        if (jalali) {
                            item = dayjs_min(item).calendar('jalali').format('YYYY-MM-DD');
                        }
                        if (dayjs_min(item).isBetween(selectedDays.from, date, null, '[]')) {
                            return true;
                        }
                        return false;
                    });
                    if (disables.length > 0) {
                        if (dayjs_min(selectedDays.from).isSameOrBefore(hoverDay)) {
                            disables.sort(function (prev, next) {
                                return dayjs_min(prev).isSameOrBefore(next) ? -1 : 1;
                            });
                            if (jalali) {
                                setHoverDay(dayjs_min(disables[0])
                                    .subtract(1, 'day')
                                    .calendar('jalali')
                                    .format('YYYY-MM-DD'));
                                setSelectedDays({
                                    from: selectedDays.from,
                                    to: dayjs_min(disables[0])
                                        .subtract(1, 'day')
                                        .calendar('jalali')
                                        .format('YYYY-MM-DD'),
                                });
                                handleChange({
                                    to: dayjs_min(disables[0])
                                        .subtract(1, 'day')
                                        .format('YYYY-MM-DD'),
                                    from: dayjs_min(selectedDays.from, {
                                        jalali: jalali,
                                    })
                                        .calendar('gregory')
                                        .format('YYYY-MM-DD'),
                                });
                            }
                            else {
                                setHoverDay(dayjs_min(disables[0])
                                    .subtract(1, 'day')
                                    .format('YYYY-MM-DD'));
                                setSelectedDays({
                                    from: selectedDays.from,
                                    to: dayjs_min(disables[0])
                                        .subtract(1, 'day')
                                        .format('YYYY-MM-DD'),
                                });
                                handleChange({
                                    to: dayjs_min(disables[0])
                                        .subtract(1, 'day')
                                        .format('YYYY-MM-DD'),
                                    from: selectedDays.from,
                                });
                            }
                        }
                        else {
                            disables.sort(function (prev, next) {
                                return dayjs_min(prev).isSameOrBefore(next) ? 1 : -1;
                            });
                            if (jalali) {
                                setHoverDay(dayjs_min(disables[0])
                                    .add(1, 'day')
                                    .calendar('jalali')
                                    .format('YYYY-MM-DD'));
                                setSelectedDays({
                                    to: selectedDays.from,
                                    from: dayjs_min(disables[0])
                                        .add(1, 'day')
                                        .calendar('jalali')
                                        .format('YYYY-MM-DD'),
                                });
                                handleChange({
                                    from: dayjs_min(disables[0])
                                        .add(1, 'day')
                                        .format('YYYY-MM-DD'),
                                    to: dayjs_min(selectedDays.from, {
                                        jalali: jalali,
                                    })
                                        .calendar('gregory')
                                        .format('YYYY-MM-DD'),
                                });
                            }
                            else {
                                setHoverDay(dayjs_min(disables[0]).add(1, 'day').format('YYYY-MM-DD'));
                                setSelectedDays({
                                    to: selectedDays.from,
                                    from: dayjs_min(disables[0])
                                        .add(1, 'day')
                                        .format('YYYY-MM-DD'),
                                });
                                handleChange({
                                    from: dayjs_min(disables[0])
                                        .add(1, 'day')
                                        .format('YYYY-MM-DD'),
                                    to: selectedDays.from,
                                });
                            }
                        }
                    }
                    else {
                        setHoverDay(date);
                        if (dayjs_min(selectedDays.from).isSameOrBefore(hoverDay)) {
                            setSelectedDays({
                                from: selectedDays.from,
                                to: date,
                            });
                        }
                        else {
                            setSelectedDays({
                                to: selectedDays.from,
                                from: date,
                            });
                        }
                        returnHandleChange(jalali, date);
                    }
                }
                else {
                    // if disableDate not exist
                    setSelectedDays({
                        from: selectedDays.from,
                        to: hoverDay,
                    });
                    setHoverDay(date);
                    returnHandleChange(jalali, date);
                }
                setFirstClick(true);
                setStateOfRange(false);
            }
            return [2 /*return*/, false];
        });
    }); };
    var returnHandleChange = function (jalali, date) { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, dayjs_min(selectedDays.from).isSameOrBefore(date)];
                case 1:
                    if (_a.sent()) {
                        if (jalali) {
                            handleChange({
                                from: dayjs_min(selectedDays.from, {
                                    jalali: jalali,
                                })
                                    .calendar('gregory')
                                    .format('YYYY-MM-DD'),
                                to: dayjs_min(date, { jalali: jalali })
                                    .calendar('gregory')
                                    .format('YYYY-MM-DD'),
                            });
                        }
                        else {
                            handleChange({
                                from: selectedDays.from,
                                to: date,
                            });
                        }
                    }
                    else {
                        if (jalali) {
                            handleChange({
                                to: dayjs_min(selectedDays.from, {
                                    jalali: jalali,
                                })
                                    .calendar('gregory')
                                    .format('YYYY-MM-DD'),
                                from: dayjs_min(date, { jalali: jalali })
                                    .calendar('gregory')
                                    .format('YYYY-MM-DD'),
                            });
                        }
                        else {
                            handleChange({
                                to: selectedDays.from,
                                from: date,
                            });
                        }
                    }
                    return [2 /*return*/];
            }
        });
    }); };
    var hoverDays = function (day) { return __awaiter(void 0, void 0, void 0, function () {
        var date;
        return __generator(this, function (_a) {
            date = day.currentTarget.getAttribute('data-date');
            if (!disabled) {
                if (stateOfRange) {
                    setHoverDay(date);
                    setSelectedDays(__assign(__assign({}, selectedDays), { to: date }));
                }
            }
            return [2 /*return*/];
        });
    }); };
    var handleHoverDays = function (day) {
        // check if user want select range
        if (selectedDays.from.length === 0) {
            return false;
        }
        if (stateOfRange) {
            if (day.isBefore(dayjs_min(hoverDay, { jalali: jalali }).calendar('gregory'))) {
                return day.isBetween(dayjs_min(selectedDays.from, {
                    jalali: jalali,
                }).calendar('gregory'), dayjs_min(hoverDay, { jalali: jalali }).calendar('gregory'), null, '[]');
            }
            else {
                return day.isBetween(dayjs_min(hoverDay, { jalali: jalali }).calendar('gregory'), dayjs_min(selectedDays.from, {
                    jalali: jalali,
                }).calendar('gregory'), null, '[]');
            }
        }
        else {
            return day.isBetween(dayjs_min(selectedDays.from, {
                jalali: jalali,
            }).calendar('gregory'), dayjs_min(selectedDays.to, {
                jalali: jalali,
            }).calendar('gregory'), null, '[]');
        }
    }; // end handleHoverDays
    var handleDisabledDate = function (day) {
        var date = dayjs_min(day).format('YYYY-MM-DD');
        if (jalali) {
            date = dayjs_min(day, { jalali: jalali }, 'YYYY-MM-DD')
                .calendar('gregory')
                .format('YYYY-MM-DD');
        }
        if (!disabledBeforToday)
            return false;
        return (disabledDays.includes(date) || dayjs_min(dayjs_min(date)).isBefore(dayjs_min(), 'day'));
    };
    return (createElement(DayStyle, { className: classNames({
            inactive: day.month() !== source.subtract(-month, 'month').month(),
            'range-select': handleHoverDays(day),
            disabled: handleDisabledDate(day),
            'start-date': dayjs_min(day).format('YYYY-MM-DD') ===
                (dayjs_min(selectedDays.from).isSameOrBefore(selectedDays.to)
                    ? selectedDays.from
                    : selectedDays.to),
            'end-date': dayjs_min(day).format('YYYY-MM-DD') ===
                (dayjs_min(selectedDays.from).isSameOrBefore(selectedDays.to)
                    ? selectedDays.to
                    : selectedDays.from),
            jalali: jalali,
            today: dayjs_min()
                .calendar(jalali ? 'jalali' : 'gregory')
                .format('YYYY-MM-DD') === day.format('YYYY-MM-DD'),
            same: dayjs_min(selectedDays.from).isSame(selectedDays.to),
        }), "data-date": day.format('YYYY-MM-DD'), onClick: function (day) { return handleClick(day.currentTarget.getAttribute('data-date')); }, onMouseEnter: function (e) { return (hoverable ? hoverDays(e) : null); } }, DayComponent ? (createElement(DayComponent, { day: day })) : (createElement("p", { className: "date" }, day.format('DD')))));
};

var Month$1 = function () {
    var _a = useContext(Context), numberOfMonths = _a.numberOfMonths, jalali = _a.jalali, source = _a.source, TitleComponent = _a.TitleComponent;
    var getMonth = function () {
        var months = [];
        var _loop_1 = function (i) {
            if (source.get('day') === 0) {
                source = source.add(1, 'day');
            }
            months.push(createElement("div", { className: "month", key: Math.random() },
                TitleComponent ? (createElement(TitleComponent, { source: source })) : (createElement(TitleOfWeek, { jalali: jalali })),
                createElement(Calendar, { source: source.add(i, 'month'), jalali: jalali }, function (calendar) { return (createElement(Box, { column: true }, calendar.map(function (week) { return (createElement(Box, { className: "week", key: Math.random() }, week.map(function (day) {
                    return (createElement(Day$1, { key: day.format('YYYY-MM-DD'), day: day, month: i }));
                }))); }))); })));
        };
        for (var i = 0; i < numberOfMonths; i++) {
            _loop_1(i);
        }
        return months;
    };
    var memoGetMonth = useMemo(function () { return getMonth(); }, undefined);
    return (createElement(MonthsStyle, { numberOfMonths: numberOfMonths || 1, jalali: jalali }, memoGetMonth));
};

var defaultSelectedDays = {
    from: '',
    to: '',
};
var RangePicker = function (_a) {
    var _b = _a.jalali, jalali = _b === void 0 ? false : _b, _c = _a.numberOfMonths, numberOfMonths = _c === void 0 ? 1 : _c, handleChange = _a.handleChange, _d = _a.selectedDays, selectedDays = _d === void 0 ? defaultSelectedDays : _d, _e = _a.disabled, disabled = _e === void 0 ? false : _e, _f = _a.disabledDays, disabledDays = _f === void 0 ? [] : _f, responsive = _a.responsive, _g = _a.hoverable, hoverable = _g === void 0 ? true : _g, dayComponent = _a.dayComponent, _h = _a.disabledBeforToday, disabledBeforToday = _h === void 0 ? true : _h, titleComponent = _a.titleComponent;
    var _j = useState(false), stateOfRange = _j[0], setStateOfRange = _j[1];
    var _k = useState(null), hoverDay = _k[0], setHoverDay = _k[1];
    var _l = useState(true), firstClick = _l[0], setFirstClick = _l[1];
    var _m = useState(numberOfMonths), numberOfMonth = _m[0], setNumberOfMonth = _m[1];
    var _o = useState(false), displayMonths = _o[0], setDisplayMonths = _o[1];
    var _p = useState({
        from: '',
        to: '',
    }), cloneSelectedDays = _p[0], setSelectedDays = _p[1];
    var _q = useState(dayjsLocalized(jalali)), source = _q[0], setSource = _q[1];
    useEffect(function () {
        if (selectedDays.from) {
            var diffDay = 0;
            diffDay = dayjs_min(selectedDays.from).diff(dayjs_min().format('YYYY-MM-DD'), 'day');
            setSource(dayjs_min()
                .add(diffDay, 'day')
                .calendar(jalali ? 'jalali' : 'gregory')
                .locale(jalali ? 'fa' : 'en'));
        }
        else {
            setSource(dayjsLocalized(jalali));
        }
        return function () { };
    }, [jalali]);
    useEffect(function () {
        if (selectedDays.from.length > 0) {
            if (jalali) {
                setSelectedDays({
                    from: dayjs_min(selectedDays.from)
                        .calendar('jalali')
                        .format('YYYY-MM-DD'),
                    to: dayjs_min(selectedDays.to)
                        .calendar('jalali')
                        .format('YYYY-MM-DD'),
                });
            }
            else {
                setSelectedDays({
                    from: selectedDays.from,
                    to: selectedDays.to,
                });
            }
        }
        return function () { };
    }, [jalali]);
    useEffect(function () {
        var handleResize = function () {
            responsive(setNumberOfMonth);
        };
        if (responsive) {
            responsive(setNumberOfMonth);
            if (typeof window !== 'undefined') {
                window.addEventListener('resize', handleResize);
            }
        }
        else {
            setNumberOfMonth(numberOfMonths);
        }
        return function () {
            if (typeof window !== 'undefined') {
                window.removeEventListener('resize', handleResize);
            }
        };
    }, [numberOfMonths]);
    return (createElement("div", { className: "tp-calendar" },
        createElement(Context.Provider, { value: {
                DayComponent: dayComponent,
                TitleComponent: titleComponent,
                handleChange: handleChange,
                selectedDays: cloneSelectedDays,
                disabledDays: disabledDays,
                disabled: disabled,
                setSelectedDays: setSelectedDays,
                displayMonths: displayMonths,
                setDisplayMonths: setDisplayMonths,
                numberOfMonths: numberOfMonth,
                jalali: jalali,
                source: source,
                setSource: setSource,
                stateOfRange: stateOfRange,
                setStateOfRange: setStateOfRange,
                hoverDay: hoverDay,
                setHoverDay: setHoverDay,
                firstClick: firstClick,
                setFirstClick: setFirstClick,
                hoverable: hoverable,
                disabledBeforToday: disabledBeforToday,
            } },
            createElement(Header, null),
            displayMonths ? createElement(DisplayMonth, null) : createElement(Month$1, null))));
};

var fontSize = function (fs) { return fs / 14 + 'rem'; };
var theme = {
    primary: '#13c8b5',
    primaryHover: '#12baa9',
    secondary: '#757575',
    underlineColor: '#d0f4f0',
    disabledText: '#E0E0E0',
    dir: 'rtl',
    fs16: fontSize(16),
    fs14: fontSize(14),
    fs13: fontSize(13),
    fs12: fontSize(12),
    fs11: fontSize(11),
};

export { DatePicker, RangePicker, theme };
//# sourceMappingURL=index.es.js.map
