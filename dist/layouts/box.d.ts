import * as React from "react";
export interface BoxProps extends React.HTMLProps<HTMLDivElement> {
    component?: string;
    column?: boolean;
    order?: number;
    flex?: string;
    flexWrap?: boolean;
    reverse?: boolean;
}
declare const Box: import("styled-components").StyledComponent<"div", any, BoxProps, never>;
export default Box;
