import * as React from 'react';
export interface PortalProps {
    children: React.ReactNode;
    node?: Element;
}
declare class Portal extends React.Component<PortalProps> {
    defaultNode: Element;
    componentWillUnmount(): void;
    render(): React.ReactPortal;
}
export default Portal;
