import * as React from 'react';
export interface FieldSimulatorProps {
    component: any;
    [otherProps: string]: any;
}
export declare const FieldSimulator: React.FunctionComponent<FieldSimulatorProps>;
