export declare const align: (props: any) => "left" | "right";
export declare const isDirection: (props: any) => any;
export declare const getSize: (props: any, param1: any, param2: any) => any;
export declare const reverseAlign: (props: any) => "left" | "right";
