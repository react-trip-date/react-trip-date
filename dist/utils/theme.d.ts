declare const theme: {
    primary: string;
    primaryHover: string;
    secondary: string;
    underlineColor: string;
    disabledText: string;
    dir: string;
    fs16: string;
    fs14: string;
    fs13: string;
    fs12: string;
    fs11: string;
};
export default theme;
